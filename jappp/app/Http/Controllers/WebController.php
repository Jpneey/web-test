<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function welcome(){
      return view('welcome');
    }
    public function about(){
      return view('about');
    }
    public function gallery(){
      return view('gallery');
    }
}
